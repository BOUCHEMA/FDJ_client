<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class HomeController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction()
    {
        $data = $this->get('api_connection')->authenticate();
        $openedGames = $this->get('api_connection')->getOpenedGames($data->access_token);

        return $this->render(
            'AppBundle:Home:index.html.twig',
            [
                'carousel' => [
                    'data' => $openedGames,
                    'id'   => 'carousel_illiko',
                ]
            ]
        );
    }
}
